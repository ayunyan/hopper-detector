package jp.minecraft.hopperdetector.listener;

import jp.minecraft.hopperdetector.HopperDetectorPlugin;
import org.bukkit.Location;
import org.bukkit.block.Hopper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * User: ayu
 * Date: 14/03/09
 * Time: 19:31
 */
public class InventoryListener implements Listener {
    private final HopperDetectorPlugin plugin;

    public InventoryListener(HopperDetectorPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
        if (!plugin.isActive()) return;
        Inventory initiator = event.getInitiator();
        InventoryHolder holder = initiator.getHolder();

        if (holder instanceof Hopper) {
            Location location = ((Hopper) holder).getLocation();
            plugin.addRecord(location);
        }
    }
}
