package jp.minecraft.hopperdetector;

import jp.commun.minecraft.util.command.CommandManager;
import jp.commun.minecraft.util.command.CommandPermissionException;
import jp.minecraft.hopperdetector.command.PluginCommand;
import jp.minecraft.hopperdetector.listener.InventoryListener;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * User: ayu
 * Date: 14/03/09
 * Time: 19:29
 */
public class HopperDetectorPlugin extends JavaPlugin {
    private CommandManager commandManager;
    private Map<Location, MutableInt> records = new HashMap<Location, MutableInt>();
    private boolean active = false;

    @Override
    public void onEnable() {
        super.onEnable();

        commandManager = new CommandManager();
        commandManager.register(new PluginCommand(this));

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new InventoryListener(this), this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            commandManager.execute(sender, command, args);
        } catch (CommandPermissionException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to do this.");
        } catch (CommandException e) {
            sender.sendMessage(e.getMessage());
        }

        return false;
    }

    public void addRecord(Location location) {
        MutableInt counter = records.get(location);
        if (counter == null) {
            counter = new MutableInt();
            records.put(location, counter);
        }
        counter.increment();
    }

    public Map<Location, MutableInt> getRecords() {
        return records;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
