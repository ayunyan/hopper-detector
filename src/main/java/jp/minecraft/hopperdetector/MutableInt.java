package jp.minecraft.hopperdetector;

/**
 * User: ayu
 * Date: 14/03/09
 * Time: 19:38
 */
public class MutableInt {
    private int value = 0;

    public MutableInt() {
    }

    public MutableInt(int value) {
        this.value = value;
    }

    public void increment() {
        value++;
    }

    public void decrement() {
        value--;
    }

    public void reset() {
        value = 0;
    }

    public void add(int v) {
        value += v;
    }

    public int getValue() {
        return value;
    }
}
