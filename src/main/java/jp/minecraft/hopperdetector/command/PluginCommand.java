package jp.minecraft.hopperdetector.command;

import jp.commun.minecraft.util.command.Command;
import jp.commun.minecraft.util.command.CommandHandler;
import jp.minecraft.hopperdetector.HopperDetectorPlugin;
import jp.minecraft.hopperdetector.MutableInt;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import java.util.*;

/**
 * User: ayu
 * Date: 14/03/09
 * Time: 19:47
 */
public class PluginCommand implements CommandHandler {
    private final HopperDetectorPlugin plugin;

    public PluginCommand(HopperDetectorPlugin plugin) {
        this.plugin = plugin;
    }

    @Command( names = {"hd show"}, permissions = {"hopperdetector.admin"} )
    public void show(CommandSender sender, String commandName, String[] args) {
        int limit = 10;
        if (args.length > 0) {
            limit = Integer.parseInt(args[0], 10);
        }

        List<Map.Entry<Location, MutableInt>> entries = new ArrayList<Map.Entry<Location, MutableInt>>(plugin.getRecords().entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<Location, MutableInt>>() {
            @Override
            public int compare(Map.Entry<Location, MutableInt> o1, Map.Entry<Location, MutableInt> o2) {
                return ((Integer) o2.getValue().getValue()).compareTo(o1.getValue().getValue());
            }
        });

        sender.sendMessage(ChatColor.DARK_AQUA + "===== High Frequency Hoppers =====");

        for (Map.Entry<Location, MutableInt> entry: entries) {
            if (limit <= 0) break;

            Location location = entry.getKey();
            sender.sendMessage(location.getWorld().getName() + ", " + String.valueOf(location.getX()) + ", " + String.valueOf(location.getY()) + ", " +
            String.valueOf(location.getZ()) + ": " + String.valueOf(entry.getValue().getValue()));

            limit--;
        }
    }

    @Command( names = {"hd clear"}, permissions = {"hopperdetector.admin"})
    public void clear(CommandSender sender, String commandName, String[] args) {
        plugin.getRecords().clear();

        sender.sendMessage(ChatColor.YELLOW + "record cleared.");
    }

    @Command( names = "hd on", permissions = {"hopperdetector.admin"})
    public void on(CommandSender sender, String commandName, String[] args) {
        plugin.getRecords().clear();
        plugin.setActive(true);
        sender.sendMessage(ChatColor.YELLOW + "Hopper Detector activated.");
    }

    @Command( names = "hd off", permissions = {"hopperdetector.admin"})
    public void off(CommandSender sender, String commandName, String[] args) {
        plugin.setActive(false);
        sender.sendMessage(ChatColor.YELLOW + "Hopper Detector deactivated.");
    }
}
